var gulp = require('gulp');
var sass = require('gulp-sass');
var jshint = require("gulp-jshint");
var stylish = require("jshint-stylish");
var csslint = require("gulp-csslint");
gulp.task("default", function () {
    console.log("Gulp Gulp");
});
gulp.task("compile:js", function () {
    //var bundle = browserify("./src/js/mian.js").bundle();
    console.log("compile:css");
});
gulp.task("compile:css", function () {
    console.log("Compile:css");
    gulp.src(["./scss/*.scss"]) //
        .pipe(sass()) //
        .pipe(csslint()) //
        .pipe(csslint.formatter()) //
        .pipe(gulp.dest("./public/assets/css")); //
});
gulp.task("jshint", function () {
    console.log("Jshint");
    gulp.src(["./public/assets/js/*.js"]) //
        .pipe(jshint()) //
        .pipe(jshint.reporter("jshint-stylish"));
});